CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Author


INTRODUCTION
------------

The Commerce Raiffeisen Payment module allows you to receive credit card payments from your customers. This project integrates Raiffeisen Payment system into the Drupal Commerce payment and checkout systems. Online payments is a convenience your customers have come to expect. Accepting credit card payments from your clients has proven to increase average order sizes by as much as 40%. To start accepting payments, you will first need to contact your nearest Raiffeisen bank and ask for a merchant account. Supported credit cards are Visa and Mastercard.


REQUIREMENTS
------------

Drupal Commerce.


INSTALLATION
------------

1. Install the module as usual.
2. Go to your "Modules" and enable the Raiffeisen Payment.
3. Once you install and enable your module, you will have to go to your Drupal Commerce settings and enable 'Raiffeisen bank - card' payment method.


CONFIGURATION
-------------

1. You have to contact your Raiffeisen Bank and get all necessary data. Once you receive your details (IDs and files), you can generate security keys. In order to generate SSL keys you have to download and install Win32OpenSSL. You will get detailed instructions from your bank, how to generate security keys.

2. Go to the module settings /admin/settings/raiffeisen and upload your files and enter your IDs.


AUTHOR
------

Goran Nikolovski, https://www.drupal.org/user/3451979

Company: Studio Present, Subotica, Serbia
Website: http://www.studiopresent.com
Email: info@studiopresent.com
https://www.drupal.org/node/2294967
